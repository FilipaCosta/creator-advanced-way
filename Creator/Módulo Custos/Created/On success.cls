strProj = input.Projeto.toString();
strMarco = input.Marco.toString();

if(strProj != "")
{
	projRecord = Projetos[ID == input.Projeto];
	// Inserir custos de Pré-projeto
	if(input.TipoCusto = "Pré-projeto")
	{
		rows_Preprojeto = Collection();
		totalRows = 0; 
		atualPosicao = 0;
		for each  custoPre in PreProjetoCusto
		{
			totalRows += 1; 
		}
		aux_NumDecrementar = totalRows -1; 
		for each  custoPre in PreProjetoCusto
		{
			rowCustoPreProjeto = Projetos.CustosProj();
			numLinha = totalRows - aux_NumDecrementar; 
			if (numLinha != 0) {
				custoPre.CodLinha =   input.ID + " - Custo PreProj - " + numLinha ; 
				rowCustoPreProjeto.CodCusto = input.ID + " - Custo PreProj - " + numLinha ; 
			} else {
				custoPre.CodLinha =  input.ID + " - Custo PreProj - " + totalRows; 
				rowCustoPreProjeto.CodCusto =input.ID + " - Custo PreProj - " + totalRows;
			}
			aux_NumDecrementar += 1; 
			rowCustoPreProjeto.Area=input.Area;
			rowCustoPreProjeto.Marco=input.Marco.NomeMarco;
			rowCustoPreProjeto.TipoCusto=input.TipoCusto;
			rowCustoPreProjeto.Valor=custoPre.Valor;
			rowCustoPreProjeto.Colaborador=zoho.loginuser;
			rowCustoPreProjeto.Data=custoPre.Data;
			rowCustoPreProjeto.Descricao="*" + custoPre.Nome + "* - " + custoPre.Descricao;
			rows_Preprojeto.insert(rowCustoPreProjeto);
			
			if(custoPre.Valor != null)
			{
				if(projRecord.MontanteGasto != null)
				{
					projRecord.MontanteGasto=projRecord.MontanteGasto + custoPre.Valor;
				}
				else
				{
					projRecord.MontanteGasto=custoPre.Valor;
				}
			}
		}
		projRecord.CustosProj.insert(rows_Preprojeto);
	}
	// Inserir custos de Registo Laboral
	else if(input.TipoCusto = "Registo Laboral")
	{
		
		auxHorasTotais = 0.00;
		totalRows = 0; 
		atualPosicao = 0;
		rows_Laborais = Collection();
		if(strMarco != "")
		{
			marcoRecord = Marcos[ID = input.Marco];
			tasklist = marcoRecord.Tarefas;
		}
		for each  custoLab in CustosLaborais
		{
			totalRows += 1; 
		}
		aux_NumDecrementar = totalRows -1; 
		for each  custoLaboral in CustosLaborais
		{
			rowCustoLaboral = Projetos.CustosProj();
			numLinha = totalRows - aux_NumDecrementar; 
			if (numLinha != 0) {
				custoLaboral.CodLinha =  input.ID + " - Custo Laboral -" + numLinha; 
				rowCustoLaboral.CodCusto = input.ID + " - Custo Laboral -"  + numLinha;
			} else {
				custoLaboral.CodLinha =  input.ID + " - Custo Laboral -"  + totalRows; 
				rowCustoLaboral.CodCusto = input.ID + " - Custo Laboral -"  + totalRows;
			}
			aux_NumDecrementar += 1; 
			
			rowCustoLaboral.Tarefa=custoLaboral.Tarefa;
			rowCustoLaboral.Area=input.Area;
			rowCustoLaboral.Marco=input.Marco.NomeMarco;
			rowCustoLaboral.TipoCusto=input.TipoCusto;
			rowCustoLaboral.Valor=custoLaboral.ValorLaboral;
			rowCustoLaboral.Colaborador=zoho.loginuser;
			rowCustoLaboral.Data=custoLaboral.Data;
			rows_Laborais.insert(rowCustoLaboral);
			if(custoLaboral.ValorLaboral != null)
			{
				if(projRecord.MontanteGasto != null)
				{
					projRecord.MontanteGasto=projRecord.MontanteGasto + custoLaboral.ValorLaboral;
				}
				else
				{
					projRecord.MontanteGasto=custoLaboral.ValorLaboral;
				}
			}
			if(strMarco != "")
			{
				if(marcoRecord.MontanteGasto != null)
				{
					marcoRecord.MontanteGasto=marcoRecord.MontanteGasto + custoLaboral.ValorLaboral;
				}
				else
				{
					marcoRecord.MontanteGasto=custoLaboral.ValorLaboral;
				}
				for each  task in tasklist
				{
					if(task.Tarefa == custoLaboral.Tarefa)
					{
						if(task.HorasGastas != null)
						{
							task.HorasGastas=task.HorasGastas + custoLaboral.RegistoHoras;
						}
						else
						{
							task.HorasGastas=custoLaboral.RegistoHoras;
						}
					}
				}
			}
		}
		projRecord.CustosProj.insert(rows_Laborais);
	}
	// Inserir custos de Visita Local
	else
	{
		
		rows_Visitas = Collection();
		if(strMarco != "")
		{
			marcoRecord = Marcos[ID = input.Marco];
		}
		totalRows = 0; 
		atualPosicao = 0;
		for each  custoVisitas in CustosVisitas
		{
			totalRows += 1; 
		}
		aux_NumDecrementar = totalRows -1; 
		for each  custoVisitas in CustosVisitas
		{
			rowCustoVisitas = Projetos.CustosProj();
			numLinha = totalRows - aux_NumDecrementar; 
			if (numLinha != 0) {
				custoVisitas.CodLinha =  input.ID + " - Custo de visita -" + numLinha; 
				rowCustoVisitas.CodCusto = input.ID + " - Custo de visita -" + numLinha;
			} else {
				custoVisitas.CodLinha =  input.ID + " - Custo de visita -" + totalRows; 
				rowCustoVisitas.CodCusto = input.ID + " - Custo de visita -" + totalRows;
			}
			aux_NumDecrementar += 1;
			rowCustoVisitas.Area=input.Area;
			rowCustoVisitas.Marco=input.Marco.NomeMarco;
			rowCustoVisitas.TipoCusto=input.TipoCusto;
			rowCustoVisitas.Valor=custoVisitas.ValorLinha;
			rowCustoVisitas.Colaborador=zoho.loginuser;
			rowCustoVisitas.Descricao= "*Km's percorridos* :  " 
															 + custoVisitas.KmsPercorridos + 
															 "--" + "*Alimentação* : " 
															 + custoVisitas.Alimentacao 
															 + "--" + "*Estadia* : "
															  + custoVisitas.Estadia 
															  + "--" + "* Estacionamento* : " 
															  + custoVisitas.Estacionamento 
															  + "--" 
															  + "*Valor de portagens* :" 
															  + custoVisitas.Portagens;
			if(custoVisitas.Inicio != null)
			{
				rowCustoVisitas.Data=custoVisitas.Inicio.toDate();
			}
			rows_Visitas.insert(rowCustoVisitas);
			if(custoVisitas.ValorLinha != null)
			{
				if(projRecord.MontanteGasto != null)
				{
					projRecord.MontanteGasto=projRecord.MontanteGasto + custoVisitas.ValorLinha;
				}
				else
				{
					projRecord.MontanteGasto=custoVisitas.ValorLinha;
				}
			}
			if(strMarco != "")
			{
				if(marcoRecord.MontanteGasto != null)
				{
					marcoRecord.MontanteGasto=marcoRecord.MontanteGasto + custoVisitas.ValorLinha;
				}
				else
				{
					marcoRecord.MontanteGasto=custoVisitas.ValorLinha;
				}
			}
		}
		projRecord.CustosProj.insert(rows_Visitas);
	}
}
