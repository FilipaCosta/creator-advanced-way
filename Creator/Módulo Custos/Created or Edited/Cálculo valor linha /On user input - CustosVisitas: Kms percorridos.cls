// On User input - Km's percorridos:  
aux_ValorLinha = 0.00;
empName = thisapp.permissions.loginUserName();
precoKmColaborador = Colaboradores[Colaborador == empName].PrecoKm;
if(row.KmsPercorridos != null)
{
	row.ValoKms=row.KmsPercorridos * precoKmColaborador;
	aux_ValorLinha = aux_ValorLinha + row.ValoKms;
}
else
{
	row.ValoKms=0.00;
}
if(row.Alimentacao != null)
{
	aux_ValorLinha = aux_ValorLinha + row.Alimentacao;
}
if(row.Estadia != null)
{
	aux_ValorLinha = aux_ValorLinha + row.Estadia;
}
if(row.Estacionamento != null)
{
	aux_ValorLinha = aux_ValorLinha + row.Estacionamento;
}
if(row.ValorPortagens != null)
{
	aux_ValorLinha = aux_ValorLinha + row.ValorPortagens;
}
row.ValorLinha=aux_ValorLinha;
