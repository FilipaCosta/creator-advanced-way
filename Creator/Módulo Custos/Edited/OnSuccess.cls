strProj = input.Projeto.toString();
strMarco = input.Marco.toString();
if(strProj != "")
{
	projRecord = Projetos[ID == input.Projeto];
	custosProjeto = projRecord.CustosProj;
	if(input.TipoCusto = "Pré-projeto")
	{
		aux_NumIncrementar = 1;
		totalRows = 0;
		for each  custoPre in PreProjetoCusto
		{
			/* Retornar todas as linhas do subform */
			if(custoPre.CodLinha != null)
			{
				totalRows = totalRows + 1;
			}
			/* Tratar das edições às linhas */
			if(custoPre.Editado == true)
			{
				for each  c in custosProjeto
				{
					if(c.CodCusto == custoPre.CodLinha)
					{
						c.Area=input.Area;
						c.Marco=input.Marco.NomeMarco;
						c.TipoCusto=input.TipoCusto;
						c.Valor=custoPre.Valor;
						c.Colaborador=zoho.loginuser;
						c.Data=custoPre.Data;
						c.Descricao="*" + custoPre.Nome + "* - " + custoPre.Descricao;
						if(custoPre.BudgetOld != custoPre.Valor)
						{
							projRecord.MontanteGasto=projRecord.MontanteGasto - custoPre.BudgetOld;
							projRecord.MontanteGasto=projRecord.MontanteGasto + custoPre.Valor;
						}
						custoPre.BudgetOld=null;
					}
				}
				custoPre.Editado=false;
			}
			/* Tratar das novas linhas */
			if(custoPre.CodLinha == null)
			{
				listaCustos = Collection();
				newCustoProj = Projetos.CustosProj();
				numberLine = totalRows + aux_NumIncrementar;
				custoPre.CodLinha=input.ID + " - Custo PreProj - " + numberLine;
				newCustoProj.CodCusto=input.ID + "- Custo PreProj - " + numberLine;
				aux_NumIncrementar = aux_NumIncrementar + 1;
				// Inserir novo custo
				newCustoProj.Area=input.Area;
				newCustoProj.Marco=input.Marco.NomeMarco;
				newCustoProj.TipoCusto=input.TipoCusto;
				newCustoProj.Valor=custoPre.Valor;
				newCustoProj.Colaborador=zoho.loginuser;
				newCustoProj.Data=custoPre.Data;
				newCustoProj.Descricao="*" + custoPre.Nome + "* - " + custoPre.Descricao;
				listaCustos.insert(newCustoProj);
				// Atulizar Budget 
				projRecord.MontanteGasto=projRecord.MontanteGasto + custoPre.Valor;
			}
		}
		projRecord.CustosProj.insert(listaCustos);
	}
	/* ------------------------------------------------------- Registo Laboral ---------------------------------------------------------*/
	else if(input.TipoCusto = "Registo Laboral")
	{
		if(strMarco != "")
		{
			marcoRecord = Marcos[ID = input.Marco];
			taskList = marcoRecord.Tarefas;
		}
		totalRows = 0;
		aux_NumIncrementar = 1;
		for each  cLab in CustosLaborais
		{
			if(cLab.CodLinha != null)
			{
				totalRows = totalRows + 1;
			}
			/* Tratar das edições às linhas */
			if(cLab.Editado == true)
			{
				for each  c in custosProjeto
				{
					if(c.CodCusto == cLab.CodLinha)
					{
						c.Tarefa=cLab.Tarefa;
						c.Area=input.Area;
						c.Marco=input.Marco.NomeMarco;
						c.TipoCusto=input.TipoCusto;
						c.Valor=cLab.ValorLaboral;
						c.Colaborador=zoho.loginuser;
						c.Data=cLab.Data;
					}
				}
				if(cLab.HorasAntigas != cLab.RegistoHoras)
				{
					for each  t in taskList
					{
						if(t.Tarefa == cLab.Tarefa)
						{
							t.HorasGastas=t.HorasGastas - cLab.HorasAntigas;
							t.HorasGastas=t.HorasGastas + cLab.RegistoHoras;
						}
					}
				}
				if(cLab.BudgetOld != cLab.ValorLaboral)
				{
					projRecord.MontanteGasto=projRecord.MontanteGasto - cLab.BudgetOld;
					projRecord.MontanteGasto=projRecord.MontanteGasto + cLab.ValorLaboral;
					marcoRecord.MontanteGasto=marcoRecord.MontanteGasto - cLab.BudgetOld;
					marcoRecord.MontanteGasto=marcoRecord.MontanteGasto + cLab.ValorLaboral;
				}
				cLab.Editado=false;
				cLab.HorasAntigas=null;
			}
			if(cLab.CodLinha == null)
			{
				listaCustos = Collection();
				newCustoProj = Projetos.CustosProj();
				numberLine = totalRows + aux_NumIncrementar;
				cLab.CodLinha=input.ID + " - Custo Laboral -" + numberLine;
				newCustoProj.CodCusto=input.ID + " - Custo Laboral -" + numberLine;
				aux_NumIncrementar = aux_NumIncrementar + 1;
				// Inserir novo custo
				newCustoProj.Tarefa=cLab.Tarefa;
				newCustoProj.Area=input.Area;
				newCustoProj.Marco=input.Marco.NomeMarco;
				newCustoProj.TipoCusto=input.TipoCusto;
				newCustoProj.Valor=cLab.ValorLaboral;
				newCustoProj.Colaborador=zoho.loginuser;
				newCustoProj.Data=cLab.Data;
				listaCustos.insert(newCustoProj);
				// Atualizar horas
				for each  t in taskList
				{
					if(t.Tarefa == cLab.Tarefa)
					{
						t.HorasGastas=t.HorasGastas + cLab.RegistoHoras;
					}
				}
				// Atualizar budget
				projRecord.MontanteGasto=projRecord.MontanteGasto + cLab.ValorLaboral;
				marcoRecord.MontanteGasto=marcoRecord.MontanteGasto + cLab.ValorLaboral;
			}
		}
		projRecord.CustosProj.insert(listaCustos);
	}
	/* ------------------------------------------------------- Registo Visita ---------------------------------------------------------*/
	else
	{
		if(strMarco != "")
		{
			marcoRecord = Marcos[ID = input.Marco];
		}
		totalRows = 0;
		aux_NumIncrementar = 1;
		for each cv in CustosVisitas {
			totalRows +=1;
		}
		for each  cVis in CustosVisitas
		{
			
			if(cVis.Editado == true)
			{
				for each  projCusto in custosProjeto
				{
					
					if(projCusto.CodCusto == cVis.CodLinha)
					{
						if(cVis.Inicio != null)
						{
							projCusto.Data=cVis.Inicio.toDate();
						}
						projCusto.Area=input.Area;
						projCusto.Marco=input.Marco.NomeMarco;
						projCusto.TipoCusto=input.TipoCusto;
						projCusto.Valor=cVis.ValorLinha;
						projCusto.Colaborador=zoho.loginuser;
						projCusto.Descricao="*Km's percorridos* :  " + cVis.KmsPercorridos + "--" + "*Alimentação* : " + cVis.Alimentacao + "--" + "*Estadia* : " + cVis.Estadia + "--" + "* Estacionamento* : " + cVis.Estacionamento + "--" + "*Valor de portagens* :" + cVis.Portagens;
					}
				}
				if(cVis.BudgetOld != cVis.ValorLinha)
				{
					projRecord.MontanteGasto=projRecord.MontanteGasto - cVis.BudgetOld;
					projRecord.MontanteGasto=projRecord.MontanteGasto + cVis.ValorLinha;
					marcoRecord.MontanteGasto=marcoRecord.MontanteGasto - cVis.BudgetOld;
					marcoRecord.MontanteGasto=marcoRecord.MontanteGasto + cVis.ValorLinha;
				}
				cVis.Editado=false;
			}
			if(cVis.CodLinha == null)
			{
				listaCustos = Collection();
				newCustoProj = Projetos.CustosProj();
				numberLine = totalRows;
				cVis.CodLinha=input.ID + " - Custo Visita -" + numberLine;
				newCustoProj.CodCusto=input.ID + " - Custo Visita -" + numberLine;
				aux_NumIncrementar = aux_NumIncrementar + 1;
				// Inserir novo custo
				if(cVis.Inicio != null)
				{
					newCustoProj.Data=cVis.Inicio.toDate();
				}
				newCustoProj.Area=input.Area;
				newCustoProj.Marco=input.Marco.NomeMarco;
				newCustoProj.TipoCusto=input.TipoCusto;
				newCustoProj.Valor=cVis.ValorLinha;
				newCustoProj.Colaborador=zoho.loginuser;
				newCustoProj.Descricao="*Km's percorridos* :  " + cVis.KmsPercorridos + "--" + "*Alimentação* : " + cVis.Alimentacao + "--" + "*Estadia* : " + cVis.Estadia + "--" + "* Estacionamento* : " + cVis.Estacionamento + "--" + "*Valor de portagens* :" + cVis.Portagens;
				listaCustos.insert(newCustoProj);
				projRecord.MontanteGasto=projRecord.MontanteGasto + cVis.ValorLinha;
				marcoRecord.MontanteGasto=marcoRecord.MontanteGasto + cVis.ValorLinha;
			}
		}
		projRecord.CustosProj.insert(listaCustos);
	}
}
