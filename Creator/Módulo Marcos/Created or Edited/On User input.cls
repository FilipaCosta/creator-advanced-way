strProj = input.Projeto.toString();
aux_TotalMontantesParciais = 0.00; 
if(strProj != "") {
	projeto = Projetos[ID == input.Projeto];
	if (projeto.Montante < input.MontanteParcial ) {
        alert "Montante superior ao valor total do projeto ( " + projeto.Montante + " )"; 
        input.MontanteParcial = 0.00;
    } else {
        if (input.Area != null ) {
            marco = Marcos[Area = input.Area && Projeto = input.Projeto];
            for each mark in marco {
                if (mark.MontanteParcial != null ) {
                    aux_TotalMontantesParciais += mark.MontanteParcial;  
                }
            } 
            aux_TotalMontantesParciais += input.MontanteParcial;
            if (projeto.Montante < aux_TotalMontantesParciais ) {
                aux_TotalMontantesParciais -= input.MontanteParcial;
                alert "Montante total do projeto ( " 
                        + projeto.Montante 
                        + " ) foi ultrapassado. " 
                        + "/n" 
                        + "Neste momento o total dos montantes parciais é de - " 
                        + aux_TotalMontantesParciais 
                        + "€." ; 
                input.MontanteParcial = 0.00;
            }
        } else {
            alert "Por favor selecione uma área"; 
            input.MontanteParcial = 0.00; 
        }
    }
}  else {
    alert "Por favor selecione um projeto e uma área"; 
    input.MontanteParcial = 0.00;
}
