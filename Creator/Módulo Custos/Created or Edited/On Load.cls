input.Colaborador = thisapp.permissions.loginUserName();
disable Colaborador;
if(input.TipoCusto = "")
{
	hide TituloPreprojeto;
	hide TituloVisitaLocal;
	hide TituloLaboral;
	hide PreProjetoCusto;
	hide CustosLaborais;
	hide CustosVisitas;
	hide DetalhesGerais;
}
else if(input.TipoCusto = "Pré-projeto")
{
	hide PreProjetoCusto;
	show TituloPreprojeto;
	hide TituloVisitaLocal;
	hide TituloLaboral;
	hide CustosLaborais;
	hide CustosVisitas;
	show DetalhesGerais;
}
else if(input.TipoCusto = "Registo Laboral")
{
	hide TituloPreprojeto;
	hide TituloVisitaLocal;
	show TituloLaboral;
	show CustosLaborais;
	hide PreProjetoCusto;
	hide CustosVisitas;
	show DetalhesGerais;
}
else if(input.TipoCusto = "Visita Local")
{
	hide TituloPreprojeto;
	show TituloVisitaLocal;
	hide TituloLaboral;
	hide CustosLaborais;
	hide PreProjetoCusto;
	show CustosVisitas;
	show DetalhesGerais;
}
